<?php

use App\Domain\AWS\Models\AwsConfigurator;
use App\Domain\AWS\Service\ImageStorageManager;
use App\Domain\ImageStorageManagerInterface;
use Aws\Sdk;
use Psr\Container\ContainerInterface;

return [
    AwsConfigurator::class => DI\autowire()
        ->constructor(
            DI\env('AWS_ACCESS_KEY_ID'),
            DI\env('AWS_SECRET_ACCESS_KEY'),
            DI\env('AWS_REGION', 'us-east-1'),
            DI\env('AWS_BUCKET'),
            'latest'
        ),
    ImageStorageManagerInterface::class => DI\autowire(ImageStorageManager::class),
    Sdk::class => static function (ContainerInterface $container) {
        $awsConfigurator = $container->get(AwsConfigurator::class);

        return new Sdk(
            [
                'credentials' => [
                    'key' => $awsConfigurator->getAccessKeyId(),
                    'secret' => $awsConfigurator->getSecretAccessKey(),
                ],

                'region' => $awsConfigurator->getRegion(),
                'version' => $awsConfigurator->getVersion(),
            ]
        );
    },
    MysqliDb::class => DI\autowire()
        ->constructor(
            DI\env('DB_HOST', 'localhost'),
            DI\env('DB_DATABASE'),
            DI\env('DB_USERNAME'),
            DI\env('DB_PASSWORD'),
        ),
];