<?php

use FastRoute\RouteCollector;
use Routes\Api;
use App\Core\Request;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require __DIR__.'/vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createUnsafeImmutable(__DIR__);
$dotenv->load();

$builder = new DI\ContainerBuilder();
$builder->addDefinitions('config/serviceProvider.php');
$container = $builder->build();

$dispatcher = FastRoute\simpleDispatcher(
    static function (RouteCollector $r) {
        (new Api())->createApiRoutes($r);
    }
);

// Fetch method and URI from somewhere
$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];

// Strip query string (?foo=bar) and decode URI
if (false !== $pos = strpos($uri, '?')) {
    $uri = substr($uri, 0, $pos);
}

$uri = rawurldecode($uri);

$routeInfo = $dispatcher->dispatch($httpMethod, $uri);

switch ($routeInfo[0]) {
    case FastRoute\Dispatcher::NOT_FOUND:
        echo \Gridonic\JsonResponse\ErrorJsonResponse::create([$uri => 'Route is not defined'],'Route is not defined');
        break;
    case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        $allowedMethods = $routeInfo[1];
        echo \Gridonic\JsonResponse\ErrorJsonResponse::create([$httpMethod => 'Method is not allowed'],'Method is not allowed');
        break;
    case FastRoute\Dispatcher::FOUND:
        $handler = $routeInfo[1];
        $vars = array_merge($routeInfo[2], [Request::createNewRequest()]);
        [$class, $method] = explode('::', $handler, 2);
        header("Content-Type: application/json");
        echo call_user_func_array([$container->get($class), $method], $vars);
        break;
}