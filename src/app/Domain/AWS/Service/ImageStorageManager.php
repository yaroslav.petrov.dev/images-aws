<?php

namespace App\Domain\AWS\Service;

use App\Domain\AWS\Models\AwsConfigurator;
use App\Domain\Image\Models\ImageModel;
use App\Domain\ImageStorageManagerInterface;
use Aws\Sdk;

class ImageStorageManager implements ImageStorageManagerInterface
{
    /**
     * @var AwsConfigurator
     */
    private AwsConfigurator $awsConfigurator;

    /**
     * @var Sdk
     */
    private Sdk $awsService;

    /**
     * @param AwsConfigurator $awsConfigurator
     * @param Sdk             $awsService
     */
    public function __construct(AwsConfigurator $awsConfigurator, Sdk $awsService)
    {
        $this->awsConfigurator = $awsConfigurator;
        $this->awsService = $awsService;
    }

    /**
     * @param ImageModel $model
     * @param string     $path
     *
     * @return mixed
     */
    public function saveImageByImageModel(ImageModel $model, string $path)
    {
        $s3 = $this->awsService->createS3();
        $name = $model->getName();
        $bucket = $this->awsConfigurator->getBucket();

        return $s3->upload($bucket, $name, fopen($path, 'rb'));
    }

    /**
     * @param ImageModel $model
     *
     * @return string
     */
    public function getImageUriByImageModel(ImageModel $model): string {
        $s3     = $this->awsService->createS3();
        $key    = $model->getName();
        $bucket = $this->awsConfigurator->getBucket();

        $cmd = $s3->getCommand('GetObject', [
            'Bucket' => $bucket,
            'Key'    => $key,
        ]);

        $request = $s3->createPresignedRequest($cmd, '+20 minutes');

        return (string)$request->getUri();
    }

}