<?php

namespace App\Domain\AWS\Models;

class AwsConfigurator
{
    /**
     * @var string
     */
    private string $accessKeyId;

    /**
     * @var string
     */
    private string $secretAccessKey;

    /**
     * @var string
     */
    private string $region;

    /**
     * @var string
     */
    private string $bucket;

    /**
     * @var string
     */
    private string $version;

    /**
     * @param string $key
     * @param string $secret
     * @param string $region
     * @param string $bucket
     * @param string $version
     */
    public function __construct(string $key, string $secret, string $region, string $bucket, string $version)
    {
        $this->accessKeyId = $key;
        $this->secretAccessKey = $secret;
        $this->region = $region;
        $this->bucket = $bucket;
        $this->version = $version;
    }

    /**
     * @return string
     */
    public function getAccessKeyId(): string
    {
        return $this->accessKeyId;
    }

    /**
     * @return string
     */
    public function getSecretAccessKey(): string
    {
        return $this->secretAccessKey;
    }

    /**
     * @return string
     */
    public function getRegion(): string
    {
        return $this->region;
    }

    /**
     * @return string
     */
    public function getBucket(): string
    {
        return $this->bucket;
    }

    /**
     * @return string
     */
    public function getVersion(): string
    {
        return $this->version;
    }
}