<?php

namespace App\Domain\Image\Requests;

use App\Core\NativeChildRequest;

class PostImageChildRequest extends NativeChildRequest
{
    /**
     * @return array|string[]|null
     */
    public function validateRequest(): ?array
    {
        $fileImage = $this->parentRequest->findFile('image');
        if ($fileImage === null) {
            return [
                'image' => self::REQUIRED_FIELD_MESSAGE,
            ];
        }

        return null;
    }

    /**
     * @return array
     */
    public function getImageFile(): array
    {
        return $this->parentRequest->findFile('image');
    }
}