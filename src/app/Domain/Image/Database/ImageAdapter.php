<?php

namespace App\Domain\Image\Database;

use App\Domain\Image\Models\ImageModel;

class ImageAdapter
{
    /**
     * @param array $data
     *
     * @return ImageModel
     */
    public static function mapToImageModelFromArray(array $data): ImageModel
    {
        return new ImageModel(
            $data['id'],
            $data['name'],
            $data['size'],
            $data['height'],
            $data['width'],
            $data['created_at']
        );
    }
}