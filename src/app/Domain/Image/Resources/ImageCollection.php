<?php

namespace App\Domain\Image\Resources;

use App\Core\Resources\NativeCollection;
use App\Domain\Image\Models\ImageModel;

class ImageCollection extends NativeCollection
{
    protected static string $resourceClass = ImageResource::class;

    protected static string $modelClass = ImageModel::class;
}