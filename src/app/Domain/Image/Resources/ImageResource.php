<?php

namespace App\Domain\Image\Resources;

use App\Core\Resources\NativeResource;
use App\Domain\Image\Models\ImageModel;

class ImageResource extends NativeResource
{
    /**
     * @var ImageModel
     */
    private ImageModel $model;

    /**
     * @param ImageModel $model
     */
    public function __construct(ImageModel $model)
    {
        $this->model = $model;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->model->getId(),
            'name' => $this->model->getName(),
            'size' => $this->model->getSize(),
            'height' => $this->model->getHeight(),
            'width' => $this->model->getWidth(),
            'created_at' => $this->model->getCreatedAt(),
            'predefined_uri' => $this->model->getUri(),
        ];
    }
}