<?php

namespace App\Domain\Image\Repositories;

use App\Core\NativeRepository;
use App\Domain\Image\Models\ImageModel;

class ImageRepository extends NativeRepository
{
    protected static string $tableName = 'images';

    /**
     * @return array
     * @throws \Exception
     */
    public function findAll(): array
    {
        return $this->get();
    }

    /**
     * @param int $id
     *
     * @return array
     * @throws \Exception
     */
    public function findImageById(int $id): array
    {
        $this->dbConnection->where('id', $id);

        return $this->get();
    }

    /**
     * @param ImageModel $model
     *
     * @return bool|int
     * @throws \Exception
     */
    public function saveImageByImageModel(ImageModel $model)
    {
        return $this->insert(
            [
                'id' => null,
                'name' => $model->getName(),
                'size' => $model->getSize(),
                'height' => $model->getHeight(),
                'width' => $model->getWidth(),
                'created_at' => $model->getCreatedAt(),
            ]
        );
    }
}