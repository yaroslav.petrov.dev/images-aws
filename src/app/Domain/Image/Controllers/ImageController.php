<?php

namespace App\Domain\Image\Controllers;

use App\Core\NativeController;
use App\Core\Request;
use App\Core\ValidationResponse;
use App\Domain\Image\Requests\GetImageRequest;
use App\Domain\Image\Resources\ImageCollection;
use App\Domain\Image\Resources\ImageResource;
use App\Domain\Image\Service\ImageService;
use App\Domain\Image\Requests\PostImageChildRequest;
use Gridonic\JsonResponse\StructuredJsonResponse;
use Gridonic\JsonResponse\SuccessJsonResponse;

class ImageController extends NativeController
{
    /**
     * @var ImageService
     */
    private ImageService $imageService;

    /**
     * @param ImageService $imageService
     */
    public function __construct(ImageService $imageService)
    {
        $this->imageService = $imageService;
    }

    /**
     * @param Request $request
     *
     * @return StructuredJsonResponse
     * @throws \Exception
     */
    public function getImages(Request $request): StructuredJsonResponse
    {
        $imageModels = $this->imageService->getImages();

        return SuccessJsonResponse::create(new ImageCollection($imageModels));
    }

    /**
     * @param Request $request
     *
     * @return StructuredJsonResponse
     * @throws \Exception
     */
    public function postImage(Request $request): StructuredJsonResponse
    {
        $postImageRequest = PostImageChildRequest::createFromRequest($request);
        if ($validationErrors = $postImageRequest->validateRequest()) {
            return ValidationResponse::create($validationErrors);
        }

        $imageModel = $this->imageService->create($postImageRequest->getImageFile());

        return SuccessJsonResponse::create(new ImageResource($imageModel));
    }

    /**
     * @param int     $id
     * @param Request $request
     *
     * @return StructuredJsonResponse
     * @throws \Exception
     */
    public function getImage(int $id, Request $request): StructuredJsonResponse
    {
        $imageModel = $this->imageService->getImage($id);

        return SuccessJsonResponse::create(new ImageResource($imageModel));
    }
}