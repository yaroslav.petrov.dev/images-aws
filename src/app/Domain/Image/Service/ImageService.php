<?php

namespace App\Domain\Image\Service;

use App\Domain\Image\Database\ImageAdapter;
use App\Domain\Image\Models\ImageModel;
use App\Domain\Image\Repositories\ImageRepository;
use App\Domain\ImageStorageManagerInterface;
use Exception;
use Intervention\Image\ImageManager;
use RuntimeException;

class ImageService
{
    /**
     * @var ImageRepository
     */
    private ImageRepository $imageRepository;

    /**
     * @var ImageManager
     */
    private ImageManager $imageManager;

    /**
     * @var ImageStorageManagerInterface
     */
    private ImageStorageManagerInterface $imageStorageManager;

    /**
     * @param ImageRepository              $imageRepository
     * @param ImageManager                 $imageManager
     * @param ImageStorageManagerInterface $imageStorageManager
     */
    public function __construct(
        ImageRepository $imageRepository,
        ImageManager $imageManager,
        ImageStorageManagerInterface $imageStorageManager
    ) {
        $this->imageRepository = $imageRepository;
        $this->imageManager = $imageManager;
        $this->imageStorageManager = $imageStorageManager;
    }

    /**
     * @param array $data
     *
     * @return ImageModel
     * @throws \Exception
     */
    public function create(array $data): ImageModel
    {
        $imageFile = $this->imageManager->make($data['tmp_name']);

        $model = new ImageModel(
            null,
            uniqid('', true).$data['name'],
            $data['size'],
            $imageFile->getHeight(),
            $imageFile->getWidth(),
            time(),
        );

        $id = $this->imageRepository->saveImageByImageModel($model);

        if (!$id) {
            throw new RuntimeException('Image cannot be saved to db');
        }

        $model->setId($id);

        $this->imageStorageManager->saveImageByImageModel($model, $imageFile->basePath());

        $model->setUri($this->imageStorageManager->getImageUriByImageModel($model));

        return $model;
    }

    /**
     * @return array|ImageModel[]
     * @throws Exception
     */
    public function getImages(): array
    {
        $imageData = $this->imageRepository->findAll();

        return array_map(
            static function (array $data) {
                return ImageAdapter::mapToImageModelFromArray($data);
            },
            $imageData
        );
    }

    /**
     * @param int $id
     *
     * @return ImageModel
     * @throws Exception
     */
    public function getImage(int $id): ImageModel
    {
        $imageData = $this->imageRepository->findImageById($id);

        if (empty($imageData)) {
            throw new Exception("{$id} image not found in database");
        }

        $model = ImageAdapter::mapToImageModelFromArray($imageData[0]);
        $model->setUri($this->imageStorageManager->getImageUriByImageModel($model));

        return $model;
    }
}