<?php

namespace App\Domain;

use App\Domain\Image\Models\ImageModel;

interface ImageStorageManagerInterface
{
    /**
     * @param ImageModel $model
     *
     * @param string     $path
     *
     * @return mixed
     */
    public function saveImageByImageModel(ImageModel $model, string $path);

    /**
     * @param ImageModel $model
     *
     * @return string
     */
    public function getImageUriByImageModel(ImageModel $model): string;

}