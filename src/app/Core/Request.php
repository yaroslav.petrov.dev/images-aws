<?php

namespace App\Core;

class Request
{
    /**
     * @var array
     */
    private array $getRequestVariables;

    /**
     * @var array
     */
    private array $postRequestVariables;

    /**
     * @var array
     */
    private array $serverRequestVariables;

    /**
     * @var array
     */
    private array $filesRequestVariables;

    /**
     * @var array
     */
    private array $queryParamsArray;

    /**
     * @param array $queryParamsArray
     */
    public function __construct(array $queryParamsArray)
    {
        $this->getRequestVariables = $_GET;
        $this->postRequestVariables = $_POST;
        $this->serverRequestVariables = $_SERVER;
        $this->filesRequestVariables = $_FILES;
        $this->queryParamsArray = $queryParamsArray;
    }

    /**
     * @param string $queryParams
     *
     * @return static
     */
    public static function createNewRequest($queryParams = ''): self
    {
        $queryParamsArray = [];
        if ($queryParams !== '') {
            parse_str($queryParams, $queryParamsArray);
        }

        return new self($queryParamsArray);
    }

    /**
     * @param string $key
     *
     * @return mixed
     */
    public function findGet(string $key)
    {
        if (!isset($this->getRequestVariables[$key])) {
            return null;
        }

        return $this->getRequestVariables[$key];
    }

    /**
     * @param string $key
     *
     * @return mixed
     */
    public function findPost(string $key)
    {
        if (!isset($this->postRequestVariables[$key])) {
            return null;
        }

        return $this->postRequestVariables[$key];
    }

    /**
     * @param string $key
     *
     * @return mixed
     */
    public function findFile(string $key)
    {
        if (!isset($this->filesRequestVariables[$key])) {
            return null;
        }

        return $this->filesRequestVariables[$key];
    }
}