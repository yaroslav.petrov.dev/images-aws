<?php

namespace App\Core;

abstract class NativeChildRequest
{
    public const REQUIRED_FIELD_MESSAGE = 'This field is required';

    /**
     * @var Request
     */
    protected Request $parentRequest;

    /**
     * @param Request $parentRequest
     */
    public function __construct(Request $parentRequest)
    {
        $this->parentRequest = $parentRequest;
    }

    /**
     * @param Request $request
     *
     * @return static
     */
    public static function createFromRequest(Request $request): self
    {
        return new static($request);
    }
}