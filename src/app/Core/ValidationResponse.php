<?php

namespace App\Core;

use Gridonic\JsonResponse\StructuredJsonResponse;

class ValidationResponse extends StructuredJsonResponse
{
    /**
     * {@inheritDoc}
     */
    public static function create($data = null, $message = null, $title = null, $status = 422, $headers = array())
    {
        return new static($data, $status, $headers);
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'validationError';
    }

}