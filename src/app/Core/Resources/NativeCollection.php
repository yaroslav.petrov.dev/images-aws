<?php

namespace App\Core\Resources;

use InvalidArgumentException;
use JsonSerializable;
use LogicException;

abstract class NativeCollection implements JsonSerializable
{
    private array $elements;

    protected static string $resourceClass;

    protected static string $modelClass;

    /**
     * @param array $elements
     */
    public function __construct(array $elements)
    {
        $childClass = static::class;
        if (static::$modelClass === null) {
            throw new LogicException("{$childClass} should have model className");
        }

        if (static::$resourceClass === null) {
            throw new LogicException("{$childClass} should have resource className");
        }
        $modelClass = static::$modelClass;
        foreach ($elements as $element) {
            if (!$element instanceof $modelClass) {
                throw new InvalidArgumentException("Only elements of type {$modelClass} are allowed!");
            }
        }

        $this->elements = $elements;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return array_map(
            static function ($element) {
                return new static::$resourceClass($element);
            },
            $this->elements
        );
    }
}