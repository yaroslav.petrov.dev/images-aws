<?php

namespace App\Core\Resources;

use JsonSerializable;

abstract class NativeResource implements JsonSerializable
{
    /**
     * @return array
     */
    abstract public function toArray(): array;

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return $this->toArray();
    }
}