<?php

namespace App\Core;

use LogicException;
use MysqliDb;

abstract class NativeRepository
{
    protected static string $tableName;

    /**
     * @var MysqliDb
     */
    protected MysqliDb $dbConnection;

    /**
     * @param MysqliDb $dbConnection
     */
    public function __construct(MysqliDb $dbConnection)
    {
        if (static::$tableName === '') {
            $className = static::class;
            throw new LogicException("{$className} should have tableName");
        }

        $this->dbConnection = $dbConnection;
    }

    /**
     * @param null   $numRows
     * @param string $columns
     *
     * @return array
     * @throws \Exception
     */
    protected function get($numRows = null, $columns = '*'): array
    {
        return $this->dbConnection->get(static::$tableName);
    }

    /**
     * @param array $insertData
     *
     * @return bool|int
     * @throws \Exception
     */
    protected function insert(array $insertData)
    {
        return $this->dbConnection->insert(static::$tableName, $insertData);
    }
}