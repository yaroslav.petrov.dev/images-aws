<?php

namespace Routes;

use FastRoute\RouteCollector;

class Api
{
    /**
     * @param RouteCollector $routeCollector
     */
    public function createApiRoutes(RouteCollector $routeCollector): void
    {
        $routeCollector->addGroup(
            '/api',
            static function (RouteCollector $routeCollector) {
                $routeCollector->addRoute(
                    'GET',
                    '/images',
                    'App\Domain\Image\Controllers\ImageController::getImages'
                );

                $routeCollector->addRoute(
                    'GET',
                    '/images/{id}',
                    'App\Domain\Image\Controllers\ImageController::getImage'
                );

                $routeCollector->addRoute(
                    'POST',
                    '/image',
                    'App\Domain\Image\Controllers\ImageController::postImage'
                );
            }
        );
    }
}