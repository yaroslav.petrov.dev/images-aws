CREATE DATABASE IF NOT EXISTS dev;

CREATE TABLE IF NOT EXISTS images
(
    id         INT NOT NULL AUTO_INCREMENT,
    name       VARCHAR(255),
    size       BIGINT UNSIGNED,
    width      SMALLINT UNSIGNED,
    height     SMALLINT UNSIGNED,
    created_at BIGINT UNSIGNED,
    PRIMARY KEY (ID),
    UNIQUE INDEX (name)
);