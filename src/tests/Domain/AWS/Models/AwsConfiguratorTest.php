<?php

namespace Tests\Domain\Aws\Models;

use App\Domain\AWS\Models\AwsConfigurator;
use PHPUnit\Framework\TestCase;

class AwsConfiguratorTest extends TestCase
{
    public function testAwsConfigurator(): void
    {
        $imageModel = new AwsConfigurator('test', 'test1', 'test2', 'test3', 'test4');

        self::assertEquals('test', $imageModel->getAccessKeyId());
        self::assertEquals('test1', $imageModel->getSecretAccessKey());
        self::assertEquals('test2', $imageModel->getRegion());
        self::assertEquals('test3', $imageModel->getBucket());
        self::assertEquals('test4', $imageModel->getVersion());
    }
}