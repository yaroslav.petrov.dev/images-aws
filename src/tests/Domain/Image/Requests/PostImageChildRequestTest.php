<?php

namespace Tests\Domain\Image\Requests;

use App\Core\NativeChildRequest;
use App\Core\Request;
use App\Domain\Image\Requests\PostImageChildRequest;
use PHPUnit\Framework\TestCase;

class PostImageChildRequestTest extends TestCase
{
    public function testPostImageChildRequestValidationFailed(): void
    {
        $parentRequestMock = $this->createMock(Request::class);

        $parentRequestMock->method('findFile')->willReturn(null);

        $postImageChildRequest = new PostImageChildRequest($parentRequestMock);
        self::assertEquals(
            [
                'image' => NativeChildRequest::REQUIRED_FIELD_MESSAGE,
            ],
            $postImageChildRequest->validateRequest()
        );
    }

    public function testPostImageChildRequestValidationPassed(): void
    {
        $parentRequestMock = $this->createMock(Request::class);

        $parentRequestMock->method('findFile')->willReturn(true);

        $postImageChildRequest = new PostImageChildRequest($parentRequestMock);
        self::assertEquals(null, $postImageChildRequest->validateRequest());
    }
}