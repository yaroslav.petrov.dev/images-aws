<?php

namespace Tests\Domain\Image\Models;

use App\Domain\Image\Models\ImageModel;
use PHPUnit\Framework\TestCase;

class ImageModelTest extends TestCase
{
    public function testImageModel(): void
    {
        $imageModel = new ImageModel(null, 'test', 1, 1, 1, 111);

        self::assertEquals(null, $imageModel->getId());
        self::assertEquals('test', $imageModel->getName());
        self::assertEquals(1, $imageModel->getSize());
        self::assertEquals(1, $imageModel->getHeight());
        self::assertEquals(1, $imageModel->getWidth());
        self::assertEquals(111, $imageModel->getCreatedAt());

        $imageModel->setId(1);

        self::assertEquals(1, $imageModel->getId());
    }
}