<?php

namespace Tests\Domain\Image\Repositories;

use App\Core\NativeRepository;
use Error;
use MysqliDb;
use PHPUnit\Framework\TestCase;

class NativeRepositoryTest extends TestCase
{
    public function testRepositoryWithEmptyTableName(): void
    {
        $mysqlDbMock = $this->createMock(MysqliDb::class);
        $this->expectException(Error::class);
        new TestRepository($mysqlDbMock);
    }
}

class TestRepository extends NativeRepository
{
}