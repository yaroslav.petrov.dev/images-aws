<?php

namespace Tests\Domain\Image\Repositories;

use App\Domain\Image\Repositories\ImageRepository;
use MysqliDb;
use PHPUnit\Framework\TestCase;

class ImageRepositoryTest extends TestCase
{
    public function testImageRepository(): void
    {
        $mysqlDbMock = $this->createMock(MysqliDb::class);

        $imageRepository = new ImageRepository($mysqlDbMock);

        self::assertInstanceOf(ImageRepository::class, $imageRepository);
    }
}