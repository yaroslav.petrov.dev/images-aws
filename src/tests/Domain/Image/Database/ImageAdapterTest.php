<?php

namespace Tests\Domain\Image\Database;

use App\Domain\Image\Database\ImageAdapter;
use App\Domain\Image\Models\ImageModel;
use PHPUnit\Framework\TestCase;

class ImageAdapterTest extends TestCase
{
    public function testImageAdapter(): void
    {
        $imageModel = ImageAdapter::mapToImageModelFromArray(
            [
                'id' => 1,
                'name' => 'test',
                'size' => 1,
                'height' => 1,
                'width' => 1,
                'created_at' => 111,
            ]
        );

        self::assertInstanceOf(ImageModel::class, $imageModel);
    }
}