<?php

namespace Tests\Domain\Image\Resources;

use App\Domain\Image\Models\ImageModel;
use App\Domain\Image\Resources\ImageCollection;
use PHPUnit\Framework\TestCase;

class ImageCollectionTest extends TestCase
{
    public function testImageCollection(): void
    {
        $imageResourceMock = $this->createMock(ImageModel::class);

        $imageCollection = new ImageCollection([$imageResourceMock]);

        self::assertInstanceOf(ImageCollection::class, $imageCollection);
    }
}