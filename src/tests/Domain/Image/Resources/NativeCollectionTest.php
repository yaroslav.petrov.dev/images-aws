<?php

namespace Tests\Domain\Image\Resources;

use App\Core\Resources\NativeCollection;
use Error;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class NativeCollectionTest extends TestCase
{
    public function testCollectionWithoutResourceAndModel(): void
    {
        $this->expectException(Error::class);
        new TestCollection([new TestModel()]);
    }

    public function testCollectionWithoutModel(): void
    {
        $this->expectException(Error::class);
        new TestCollectionWithResource([new TestModel()]);
    }

    public function testCollectionWithDifferentModel(): void
    {
        $this->expectException(InvalidArgumentException::class);
        new TestCollectionWithResourceAndModel([new TestOtherModel()]);
    }

    public function testCollection(): void
    {
        $collection = new TestCollectionWithResourceAndModel([new TestModel()]);
        self::assertInstanceOf(TestCollectionWithResourceAndModel::class, $collection);
    }
}

class TestCollection extends NativeCollection
{
}

class TestCollectionWithResource extends NativeCollection
{
    protected static string $resourceClass = TestResource::class;
}


class TestCollectionWithResourceAndModel extends NativeCollection
{
    protected static string $resourceClass = TestResource::class;
    protected static string $modelClass = TestModel::class;
}

class TestResource
{
}

class TestModel
{
}

class TestOtherModel
{
}