<?php

namespace Tests\Domain\Image\Resources;

use App\Domain\Image\Models\ImageModel;
use App\Domain\Image\Resources\ImageResource;
use PHPUnit\Framework\TestCase;

class ImageResourceTest extends TestCase
{
    public function testImageResource(): void
    {
        $imageModel = new ImageModel(1, 'test', 1, 1, 1, 111);
        $imageModel->setUri('testUri');

        $imageResource = new ImageResource($imageModel);

        self::assertEquals(
            [
                'id' => 1,
                'name' => 'test',
                'size' => 1,
                'height' => 1,
                'width' => 1,
                'created_at' => 111,
                'predefined_uri' => 'testUri',
            ],
            $imageResource->toArray()
        );
    }
}