### Installation

##### Configure
```bash
cd src
cp .env.example .env
```
```bash
cd ./../docker
cp .env.example .env
```
##### Configure init migration
```bash
cp ./../src/database/migrations/init.sql ./db/docker-entrypoint-initdb.d/init.sql
```

##### Create folder for database sync
```bash
mkdir db/data
```

### Running
```bash
docker-compose up --build
```

##### Install dependencies and migrate database
```bash
docker-compose exec web bash
composer install
```

##### Add virtual host to /etc/hosts
Go outside docker container and add virtual host to your /etc/hosts file

P.S. you can use localhost as domain

```bash
sudo nano /etc/hosts
# paste `127.0.0.1 images-aws` there
```

### Testing
Enter docker container to launch tests
```bash
docker-compose exec web bash
./vendor/bin/phpunit tests/
```

### Endpoints
```bash
GET /api/images
GET /api/images/{id}
POST /api/image 
    'image': <file> - required
```



